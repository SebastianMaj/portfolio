var projects = null;
var splide = null;

var projectBody = null;
var projectFilter = "all";

var isPhoneDevice = "ontouchstart" in document.documentElement; 
$(document).ready(function() {
	console.log("Mobile Device: " + isPhoneDevice);

	document.querySelector("meta[name=viewport]").setAttribute('content', 'width=device-width, initial-scale='+(1/window.devicePixelRatio));
	pixelRatio = window.devicePixelRatio;

	if(pixelRatio != 1.25 &! isPhoneDevice) {
		$('#fullpage').fullpage({
			autoScrolling:true,
			scrollHorizontally: true,
			responsiveWidth: 800
		});
	} else {
		$('#fullpage').fullpage({
			autoScrolling:false,
			scrollHorizontally: true,
			fitToSection: false,
			responsiveWidth: 800
		});
		$.fn.fullpage.setAutoScrolling(false);
	}

	// Mac adjust about us section
	if(pixelRatio == 2) {
		$(".aboutmeheader").css("padding-top", "0px");
	}

	$.fn.fullpage.setAllowScrolling(true);

	RunText();

	$("#viewworkbutton").on('click', function() { ViewWork(); });

	$( ".projectbox" ).hover(
		function() {
		  $( this ).find(".projectinfo").css("display", "block");
		}, function() {
		  $( this ).find(".projectinfo").css("display", "none");
		}
	);

	projectBody =  $("#projectslides").html();

	$.getJSON( "/projects.json", function( data ) {
		projects = data;
		LoadProjects();
	});

	var checkbox = document.querySelector('#skillslider');
	checkbox.addEventListener('change', function () {
		if (checkbox.checked) {
		  $(".webskillinfo").css("display", "none");
		  $(".webskillsbars").css("display", "none");
		  $(".gameskillinfo").css("display", "block");
		  $(".gameskillsbars").css("display", "block");
		  $(".abouthref").css("color", "#6b22bf");
		} else {
		  $(".webskillinfo").css("display", "block");
		  $(".webskillsbars").css("display", "block");
		  $(".gameskillinfo").css("display", "none");
		  $(".gameskillsbars").css("display", "none");
		  $(".abouthref").css("color", "#4895EF");
		}
	  });

	// $("#projectslides").html(projectBody);
	// $.fn.fullpage.reBuild();
});

function RefreshProjects() {
	$("#projectslides").html(projectBody);
	$.fn.fullpage.reBuild();
}

function LoadProjects() {
	RefreshProjects();

	// Take Projectslide.html and implement it into the page, first split the project array into arrays of 6 then create a slide and row then for each 3 insert into row and then repeat until you went through all projects
	var projectSort = new Array();
	projectSort[0] = new Array();
	var curIndex = 0;
	var totalIndex = 1;

	$(".projectbox").parent().css("display", "none");

	// Sort projects into groups of 6 for the slides
	for(var project in projects)
	{
		var currentProject = projects[project];
		if(projectFilter == "all" || projectFilter == currentProject.section || currentProject.section == "all") 
		{
			if(projectSort[curIndex].length >= 6)
			{
				curIndex++;
				projectSort[curIndex] = new Array();
			}

			projectSort[curIndex].push(currentProject);

			var projectElement = $("#project" + totalIndex);
			projectElement.parent().css("display", "block");
			projectElement.find(".projectname").html(currentProject.name);
			projectElement.find(".projectskills").html(currentProject.skills);
			projectElement.find(".projectimage").attr("src", "images/projects/" + currentProject.images[0]);
			projectElement.find(".projectlearnmore").attr("onclick", "ViewProject('" + project + "')")
			totalIndex++;
		}
	}

	for(var i = projectSort.length+1; i <= 3; i++) {
		$(".slide-" + i).remove();
	}

	//$(".slide-2").remove();

	// var splide = new Splide( '.splide', {
	// 	type: 'loop'
	// } ).mount();

	$.fn.fullpage.reBuild();

	$( ".projectbox" ).hover(
		function() {
		  $( this ).find(".projectinfo").css("display", "block");
		}, function() {
		  $( this ).find(".projectinfo").css("display", "none");
		}
	);
}

function ViewWork() {   	
	$("#fullpage").fullpage.moveTo(2);
}

function RunText() {
	// List of sentences
	var _CONTENT = [ "Software Developer", "Full Stack Developer", "Software Engineer", "Game Developer" ];

	// Current sentence being processed
	var _PART = 0;

	// Character number of the current sentence being processed 
	var _PART_INDEX = 0;

	// Holds the handle returned from setInterval
	var _INTERVAL_VAL;

	// Element that holds the text
	var _ELEMENT = document.querySelector("#roletext");

	// Implements typing effect
	function Type() { 
		var text =  _CONTENT[_PART].substring(0, _PART_INDEX + 1);
		_ELEMENT.innerHTML = text;
		_PART_INDEX++;

		// If full sentence has been displayed then start to delete the sentence after some time
		if(text === _CONTENT[_PART]) {
			clearInterval(_INTERVAL_VAL);
			setTimeout(function() {
				_INTERVAL_VAL = setInterval(Delete, 50);
			}, 1000);
		}
	}

	// Implements deleting effect
	function Delete() {
		var text =  _CONTENT[_PART].substring(0, _PART_INDEX - 1);
		_ELEMENT.innerHTML = text;
		_PART_INDEX--;

		// If sentence has been deleted then start to display the next sentence
		if(text === '') {
			_ELEMENT.innerHTML = "&nbsp;";
			clearInterval(_INTERVAL_VAL);

			// If last sentence then display the first one, else move to the next
			if(_PART == (_CONTENT.length - 1))
				_PART = 0;
			else
				_PART++;
			_PART_INDEX = 0;

			// Start to display the next sentence after some time
			setTimeout(function() {
				_INTERVAL_VAL = setInterval(Type, 100);
			}, 200);
		}
	}

	// Start the typing effect on load
	_INTERVAL_VAL = setInterval(Type, 100);
}

function projectfilter(filter) {
	projectFilter = filter;
	$(".projectfilter").each(function(idx, element) {
		$(element).removeClass("active");
		if($(element).attr("id") == "filter" + filter) {
			$(element).addClass("active");
		}
	});	

	LoadProjects();
}

function ViewProject(projectid) {
	var projectJson = projects[projectid];

	$("#projectpopupimg").attr("src", "images/projects/" + projectJson.images[0]);
	$(".projectpopuptitle").html(projectJson.name);
	$(".projectpopupshort").html(projectJson.short);
	$(".projectpopupdescription").html(projectJson.description);
	$(".projectpopuplink").attr("href", projectJson.link);
	
	$("#projectpopup").css("display", "block");
}

function CloseProjectView() {
	$("#projectpopup").css("display", "none");
}